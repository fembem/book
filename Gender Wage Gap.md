### The gender wage gap

It's common to see statements attributing the gender wage gap completely to sexism.

Often the implication isn't explicit, but stated implicitly by saying that the difference in pay is being "stolen", or that women are being "cheated", or by omission of any other possible casuses and explanatory factors aside from sexism.

It's entirely reasonable to assume a part of the wag gap is due to sexism, but it's quite another thing to assume all of the wage gap or almost all of it is due to sexism. And it's quite another thing to turn an extreme assumption into categorical certainty or ideological dogma.

Many reputable studies have shown that the gender gap is mostly accounted for by differing choices in professions, availability for overtime and last-minute shifts, etc. Men choose STEM (Science Technology, Engineering, and Math) careers more often than women, and they also enter more dangerous & undesirable job fields like garbage collection, sanitation, waste removal, mining, logging, roofing, welding, and fishing.

For example, [this study](https://scholar.harvard.edu/files/bolotnyy/files/be_gendergap.pdf) from researchers at Harvard University, concluded "...the weekly earnings gap can be explained by the workplace choices that women and men make. Women value time away from work and flexibility more than men..."
