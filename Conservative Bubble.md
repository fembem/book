### Conservative Fallacies

 - [You either support socialism or you suppport capitalism, and countries are either capitalist or socialist](./src/conservative_bubble/capitalism_or_socialism.md)
 - [Either socialism or capitalism must be evil](./src/conservative_bubble/duality_and_evil.md)
 - [Socialist programs just lead to more socialist programs and finally to communism](./src/conservative_bubble/socialism_slippery_slope.md)
 - [Socialism is unrepealable](./src/conservative_bubble/unrepealable_socialism_fallacy.md)
 - [Social Security and Medicare aren't socialism](./src/conservative_bubble/not_socialism.md)
 - [Scandinavian countries are capitalist](./src/conservative_bubble/scandinavians_not_socialist.md)
 - [No one has to work for minimum wage](./src/conservative_bubble/minimum_wage_escape.md)
 - [Capitalism eventually fixes all problems](./src/conservative_bubble/infallible_capitalism.md)
 - [Technology will make capitalism perfect](./src/conservative_bubble/technology_and_capitalism.md)
 - [Charter schools are proof that capitalism is better](./src/conservative_bubble/charter_schools.md)
 - [Taxation is theft, or a form of theft](./src/conservative_bubble/taxation_as_theft.md)
 - Bush didn't lie us into war until Trump said he did
 - The US is the greatest country, but half of Americans are evil
 - "liberalism is a mental disorder" & other right-wing radio gems
 - Rush Limbaugh and other cowardly monologue kings
 - the FBI & CIA are part of the deep state but they enevr did anyhting bad to foreign countries we don't get along with
 - overthrowing left-wing democracies is a good thing
 - the Republican party of today is the same as that of Lincoln
 - Simpson's Paradox and the Civil Rights Act 
 - slavery supporters were conservatives and traditionalists
 - racism, sexism, and homophobia are either insignificant or completey dominant (false dichotomy)
 - Obama was an "anti-American" "communist", but we'll pretend FDR never existed
 - Democrats always play the race card, but criticizing Israel is anti-Semitic
 - "God" doesn't appear in the Constittion
 - the pledge of allegiance was written by a socialist
 - a literal right to bear "arms" would include tanks and bombs
 - if abortion is murder, is Israel a murderous nation?
 - If Palestinians don't exist, why do Texans?
 - "self-determination" aplies to people in contiguous areas, not to religious, racial, or ethnic groups


