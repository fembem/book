The Ad-Hominem Tu-Quoque fallacy consists in asserting that an argument is of dubious value if the person making the argument does not adhere to its implications.

An example would be:
 - "How can you argue high cholesterol is dangerous for cardiac health, when you yourself have high cholesterol?"


This fallacy ignores the distinction between arguing something on the basis of personal beliefs, and arguing something on the basis of logic, data, and scientific research.

If medical research shows high cholesterol (specifically of the bad kind of cholesterol) is dangerous for one's long-term cardias health, that does not depend on the personal willpower and dietary habits of the person making an argument based on that medical research.

It's possible for a person to know something is bad or risky, and do it anyway. A large segment of the population consumes more processed sugars and fats than they know is healthy, and exercises less than they know is healthy. That doesn't mean they don't believe in medical research: it just means they have poor willpower are or procrastinating on adopting a better diet.
