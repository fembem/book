Affirming the Consequent, also know as False Contrapositive, is one of the most basic logical fallacies.

It consists in assuming that A implies B, also means that B implies A. While it's true that A and B can imply each other, usually the implication is in only in one direction.

Stated differently, it means that saying that if set A is a subset of set B (A is contained in B), then set A must be a subset of set B (B must be contained in A). While it can be the case that both "A is a subset of B" and "B is a subset of A", this only occurs when A and B are in fact the same set.

Some examples are:
 - "All men are mammals, so all mammals are men."
 - "Communists support Medicare4All, so people who support Medicare4All are communists."
 - "Racists don't support removing Confederate statues, so people who don't support removing Confederate status are racist."
 - "Sexist men prefer Bernie Sanders over ELizabeth Warren, so men who prefer Bernie Sanders over Elizabeth Warren are sexist."
  - "Republicans, conservatives, and libertarians, don't like the Clintons, so people who don't like the Clintons must be Republican, conservative, or libertarian."

To elaborate on the third point above, it could be the case that people who support Confederate statues are racist, but that can't be deduced from the reasoning above. Using illogical reasoning doesn't mean your conclusions are guaranteed to be wrong, it just means your reasoning is wrong and your reasoning doesn't support your conclusions. THhoe conclusions may well be right for other reasons though. The only way we can know for sure your conclusions are wrong, is by concluding the opposite via valid reasoning, starting from assumptions we all share and agree are true.