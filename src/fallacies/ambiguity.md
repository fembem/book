The ambiguity fallacy arises when someone uses an ambiguous term in two different ways in the same argument to reach a desired conclusion.

I like tho call this fallacy "the switcheroo" because the person using it is attempting to establish the relevance of a word using an expansive harmless definition of that word, and then to tarnish the intended target with a more restrictive but negative definition of the word.

Since many, if not most words, have different meanings, the possibilities for committing this fallacy are endless.

The key to responding to it is to point out the fallacy, but also to ask the person using it if they are ok with the same fallacy being used to tarnish people or actions they support, defend, or admire.

Some examples:
 - "Kyle Rittenhouse is a vigilante, and vigilantes are criminals, so Kyle Rittenhouse is a criminal."
 - "Kyle Rittenhouse carrying a rifle was provocative, therefore Kyle Rittenhouse provoked the assault on him by Joseph Rosenbaum."
 - "Teaching kids about LGBTQ gender theory is grooming them, and groomers are pedophiles, so people who teach kids about LGBTQ theory are pedophiles."

 In the first example, the general definition of "vigilante" as any private citizen who pursues a law and order function without compensation, is being used to easily conclude Kyle Rittenhouse is a vigilante.

Dictionary.com provides this [definition](https://www.dictionary.com/browse/vigilante) of "vigilante" as a noun:

 >any person who takes the law into his or her own hands, as by avenging a crime.

But on the same [page](https://www.dictionary.com/browse/vigilante),it also provides the definition of "vigilante" as an adjective:

 >done violently and summarily, without recourse to lawful procedures:

But then the negative connotation of the more restrictive definition of vigilantes as violent criminals, is being used to conclude Kyle Rittenhouse is a criminal.

The general definition of vigilante would include such fictional role models as The A-Team, MacGyver, The Equalizer, Knight Rider, etc. So it doesn't make sense to conclude someone is a bad person, just because they have ever been involved with a vigilante act.

Also, law and order involves protection, deterrence, investigation, prosecution, and punishment, and rehabilitation. There is a difference between vigilante protection (acting as a guard for people or property) and vigilante punishment (harming or killing someone as a form of punishment after a crime has already been committed amd when the perpetrator isn't in the process of committing a violent crime). The former is clearly a more acceptable, safe, and benign form of vigilantism, while the latter amounts to lynching.

Kyle Rittenhouse was acting as un unpaid communal property guard. He was ostensibly trying to be a deterrent to arson. He was not going around shooting or aiming his gun at the people who were starting dumpster fires, just putting those fires out himself. He only used his rifle after he was threatened and chased by Joseph Rosenbaum, a deranged person who was among other things a [convicted](https://kenoshareporter.com/stories/552689330-rosenbaum-raped-five-boys-sentencing-records-reveal) serial child rapist.

Joseph Rosenbaum had previously threatened Kyle Rittenhouse saying he'd find him and kill him when he was alone, and bizarrely enough, Rosenbaum repeatedly and loudly called Kyle Rittenhouse the n-word as seen on video and [testified](https://www.reuters.com/world/us/man-killed-by-rittenhouse-challenged-group-shoot-him-used-racial-slur-witness-2021-11-05/) to in court:

>Lackowski also said Rosenbaum was using "the N-word" while asking people to shoot him, utterances that prompted other protesters to react negatively toward him.

Regarding the second point, here is the Dictionary.com [definition](https://www.dictionary.com/browse/provocative) of "provocative":

 - tending or serving to provoke; inciting, stimulating, irritating, or vexing.

 And here is one [definition](https://www.dictionary.com/browse/provoke) of "provoke":

 >to give rise to, induce, or bring about:

 Clearly "provoke" as a verb can be used as "to cause", but "provocative" is usually meant as provoking repulsion or discomfort, not as constituting legal justification for violence or physical self-defense.