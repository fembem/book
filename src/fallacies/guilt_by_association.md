The Ad-Hominem "guilt by association" fallacy, consists in arguing that agreeing with a bad person on something means you are likely to be like that bad person.

This is asserted as if everything bad people believe is bad, or as if different people can't agree on something, but to different extents, for different reasons, or for different purposes.

Examples:
 - "Communists support Medicare4All, so if you support Medicare4All you are likely to be a communist or have communist leanings."
 - "Racists say slavery wasn't the only major motive for the Civil War, so if you say there were also other major motives for the Civil War, then you 're likely to be a racist or to sympathize with racists."
 - "Nazis were very patriotic, so if you are very patriotic, you're likely to be a Nazi or to sympathize with Nazis."
 - "The founder of the abortion provider Planned Parenthood, Margaret Sanger, was a racist who supported eugenics, so if you support abortion, you're likely to be a racist who supports eugenics."

 To elaborate on the second point Lincoln himself said in a [letter](https://www.history.com/this-day-in-history/lincoln-replies-to-horace-greeley) to abolitionist Horace Greeley:

 >“If I could save the Union without freeing any slave, I would do it; and if I could save it by freeing all the slaves, I would do it; and if I could save it by freeing some and leaving others alone, I would also do that.”

 So Lincoln himself was clear about his overriding objective in fighting the Civil War, was not ending slavery, but preserving the Union.

 In order to preserve the Union, Lincoln was willing to support guaranteeing the southern states the right to keep slavery forever.. He supported what would have become the 13th Amendment, and is now known as the [Corwin Amendment](https://en.wikipedia.org/wiki/Corwin_Amendment). It reads:

>"No amendment shall be made to the Constitution which will authorize or give to Congress power to abolish or interfere, within any State, with the domestic institutions thereof, including that of persons held to labor or service by the laws of said State."

And Lincoln didn't just support this amendment privately, he also advocated for it in his first State of the Union [address](https://avalon.law.yale.edu/19th_century/lincoln1.asp):

>"I understand a proposed amendment to the Constitution--which amendment, however, I have not seen--has passed Congress, to the effect that the Federal Government shall never interfere with the domestic institutions of the States, including that of persons held to service. To avoid misconstruction of what I have said, I depart from my purpose not to speak of particular amendments so far as to say that, holding such a provision to now be implied constitutional law, I have no objection to its being made express and irrevocable."
