## Socialism vs capitalism

Perhaps the biggest lie in American politics is the false choice between socialism and capitalism.

While much of US corporate media, and especially conservative media, present socialism vs capitalism as a binary "either or" choice, the obvious reality is that there is a socialism-capitalism spectrum that goes all the way from pure socialism, aka communism, to pure capitalism.

Instead of the more substantive discission of where we should or want to be on that spectrum on the federal, stat, and local levels, the discussion then becomes one of a false choice btween extremes, as if one were good and one were evil, and the choice had to be just one of them.

It's worth noting that the word "socialism", like most words, has multiple meanings.

The Oxford Dictionary defines socialism as:

>a political and economic theory of social organization which advocates that the means of production, distribution, and exchange should be owned or regulated by the community as a whole.

Notice the presence of the phrase "or regulated". Government regulation, while not as socialist, as full-on government ownership is still a form of socialism.

### Common attacks on the notion of socialism vs capitalism being a spectrum:


>How can socialism and capitalism be a spectrum of spcialism was invented before capitalism?

The words and philosophies behind socialism and capitalism may have been invented in deifferent times, but the fundamental aspect that the two are defined by is the degree of privatization of sectors of society. As soon as people formed societies, they had to grapple with the notion of how to share resources, and what resources were personal or common.

If you take a society that is purely socialist, and gradually privatize and then unregulate different aspects of that society, like land ownership, farming, manufacturing, fishing, transportation, etc, you would be ytraversing that spectrum, and if you take it all the way, you woud get to a society where everything is privatzed, i.e. a purely capitalist society.

And this doesn't have to apply only to modern or technologically advanced societies.

It could even apply to a small group of people stranded on an deserted island. If that group of people agree that everything they fish, hunt, forage, or farm, as well as any shelter they build, is to be shared according to their needs and not their individual productivity, then they are adopting socialism. If instead they agree that that they are each on their own in proving for themsleves, but are free to trade labor or goods with each other, then they are adopting capitalism. If they agree that they will share some things but personally possess others, or that they will all get necessary portions of everything, but more productive individuals will get addiotional portions to incentivize and reward their productivity,then they are adopting a system somewhere along the socialism-capitalism spectrum. Wheree along the spectrum they are depends on to what extent people get to consume resources based on their needs, instead of on their output and whether and to what extend thaey are allowed to accumlate provisions or have personal shelters.

>Social security, Medicare, Medicaid, etc, aren't socialism, they are just capitalis with "accomodations" , "modifications", or "adjustments".

This is clearly an attempt at playing semantic games. Those "accomodations", "modifications", or "adjustments", are with socialism. For decades Republicans generally called Medicare, Medicaid, and Social Security, "socialist" or "communist", and they currenlty say the same of proposals like Medicare4All, universal pre-school, free state college, etc.

And those "accomodations" aren't minor either. In the US, they include Social Security, Medicare, Medicaid, CHIP (Children's Health Insurance Program)the US Post Office, public schools and libraries, state colleges, subsidized housing, food assistance programs, and regulatory agencies like the FDA (Food and Drug Administration), NIH (National Institute of Health), etc. The US is clearly not a purely capitalist country, and is therefore somewhere inside the socialism-capitalism spectrum, though it is farther to the right in that spectrum than most democracies.

> Countries like Denmark, Norway, and aren't socialist, as they are ranked as having highly free markets by think tanks and studies.

This is just another attempt to false categorize countries as either socialist or capitalist, and pretend countries can't have botgh socalism and capitalism at the same time. Scandinavian countries all have universal governmenet healthcare o health insurance (which the US does not have), and generous social safety programs, paid vacation and birth leave, etc. Those are excactly the programs Republicans and conservatives say would make us socialist or commnist. While those programs wouldn't make the US socialist, and certainly not commminist, they would bring the US further to the left in the socialism-capitalism spectrum.

> Since communist countries are not democratic, socialism must be evil, and we should have as little of it as possible.

This is an example of false interpolation. Even if you assume that purely socialist or communist countries can't be democratic but that purely capitalist countries can, that doesn't mean countries with mixed economies can't be democratic. In fact, some of the most democratic countries in the world are considerably further to the left ib the socialsim-capitalism spectrum , including Scandinavian countries and most Westren European countries.

The reasoning that if ourely capitalist countries are more dmeocratic than purely socialist ones, than that means the more capitalist a country it is, the more democratic it is, commit at least two logical fallacies:
 1. that democracy is completely a function of the degree of capitalism of a society, which we know is false because countries with similar levels of capitalis can have wildly different levels of democracy, for example Saudi Arabia and the US are smilarly capitalist, but the US is much more democratic.
 2. that even if democracy, were purely a function of the degree of capitalism, that such function would have to be monotonic (always increasing  or always decreasing). If instead the function were convex (shaped like an upside-down letter "U"), then the maximun dgree of democracy would occur somewhere between pure capitalism and pure socialism, and not at either extreme.

Also, even if we assume pure socialism is worse than pure communism, that doesn't mean socialism is evil, any more than assuming burning to dealth is more painful than freezing to death would prove heat is evil or cold is divine. Just like a temprate climate is more comfortable for hiumans that searing hot temperatures or freezing cold temperatures, a location somewhere far from the extremes of the socialism-capitalism spectrum can at least theoretically be more economically and socially beneficial than either of the extremes.

>Since more capitalism has less safety nets social programs, and therefore allows more poverty, capitalism must be evil, and we should get rid of as much capitalis as possible

Unlike the previous attacks, this one is tpyically prsented by leftistsl far-leftists,or communists.

This is also an exmaple of false interpolation, falsely assuming that:

1. poverty is purely a function of the degre of capitalism, instead of also being a function of other factors, like corruption, educational sytems, cultural practices, foreign oppression, etc.
2. that, even if poverty were purely a function of the degree of capitalism, that such function has to be monotonic, i.e. either always increasing or always decresing. If instead the function were concave (shaped like the letter "U"), it would be the case that the lowest poverty level would occur somewhere inside the socialism-capitalism spectrum and not at either extreme.
