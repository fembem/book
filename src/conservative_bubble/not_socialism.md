 - Social Security and Medicare aren't socialism
   - This is an example of the "not true Scotsman" logical fallacy. I.e. saying not Scotman does something, being shown there is are one or more Scotsmen that do it, and then asserting that no "true Scotsman" does that thing. Social Security and Medicare are clear examples of socialism, and ones that are supported even by a significant proportion of conservatives. During the Obama-care legislation push, many Republicans even said they were trying to "protect" Medicare from Obama-care.

- If Medicare and Social Security are "accommodations", they're accomodations with Socialism. Word games and semantic maneuvering doesn't change the fact that Social Security and Medicare are socialist or at predomninatly socialist programs.

- Social Security & Medicare aren't socialism, just a safety net
  - This is another example of semantic maneuvering. Social Security and Medicare aren't safety nets, as all seniors are elgible for them, and aeven if they were, they would be socialist safety nets. The capitalist version of a sefety net is private savings, GoFundMe drives, and asking charitable organzaitions and churches for help.

- Regulation isn't socialism, only complete govt takeover is
   - This goes against all the rhetoric by cobservatives about "socialist regulation". Clearly regulation can be a form of socialism or partly socialist, if that regulation mandatres, helps or incentivizes collective or public control or ownership of resources, industries, etc. Just like regulations and laws can be capitalist or more capitalist than scoialist, for example ones protecting corporate owners from person liability after corporate bankruptcy, regulations can be socialist, or more socialist than capitalist, for example ones protecting consumers from excessive credit card interest rates or ATM access fees.

 - the Homestead Act was not socialism
   - it's clear that letting someone become the owner of land just because they put a fance around it and used it for farming or livestock, is at least partly socialism. The purely capitalist approach to privatizing land is to offer it to the highest bidder, not to offer it for free to the first person to use it as a home, form or ranch.