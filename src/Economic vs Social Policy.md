One of the most ambiguous ways to describe the politics of Americans and American politicians is with the terms "lefttist", "liberal", "progressive", or "conservative".

The reason these tems are ambiguous is they fail to describe or capture the fact that the left-right political spectrum has at least two major dimensions: the social one and the economic one. Other dimensions incude environemntal policy and foreign policy.

The social/cultural dimension includes such issues as:
- racism
- misogyny, sexism, and rape
- homophobia and gay rights
- transgender and non-binary gender acceptance
- police violence and abuse of power
- racism in police departments
- guns
- abortion
- the role of God or Christianity in public schools and places
- the appropriate treatment of xenophobia, bigotry, prejudice, sexism, misogyny, racism, and slavery, in public school curricula

The economic dimension includes such issues as:
 - healthcare costs such as premiums, copays, and deductibles
 - prescription drug costs, especially for tthose with high costs like cancer and diabetes patients
 - government healthcare proposals like Medicare4All
 - minimun wage increases and livable wages
 - universal pre-school
 - paid childbirth leave
 - free state college and educational justice
 - student or medical debt cancellation
 - food stamps
 - subsizided housing and housing justice
 - income tax levels and proposed wealth taxes

 Referring to an individual, a group, or a party, as "leftist", doesn't capture wether that assessment is based on econimic or socio-cultural factors.