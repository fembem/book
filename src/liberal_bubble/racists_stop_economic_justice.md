> "We can't have economic justice in the US, because racist don't want people of color to thrive"

This is one of the classic excuses those who support politicians who are allies or tool of the oligarchy, trot out to defend them.

While it's credible that racists would be willing to prolong the suffering of poor people of their own race to spite poor people of color, those racists people can't stop anything if they don't have enough votes to do so.

People who are racist presumably don't vote for Democrats and liberals or progressives anyway. So how can they be the ones stopping Medicare4All? If their numbers were big enough, they would presumably have been able to stop Obama from becoming president. And if their numbers were big enough, they would also have been able to stop Social Security and Medicare from becoming law or the party, presidents and legislators who passed them from being re-elected.

Also, conservatives and libertarians in general don't need a spite-people-of-color reason to oppose economic justice, as their ideology is opposed to the whole concept of economic justice in general, or rather they think economic justice is something that should be left to capitalism to produce automagically, not to taxation.

The other major explanation for the Democratic establishment not supporting things like Medicare4All, a wealth tax on the super-rich, free state college, etc, is that they sold out to billionaires, big banks, credit card companies, health insurers, pharmaceutical companies, etc.

This is the only explanation that makes sense.

Clearly, as long as the Democratic party doesn't support Medicare4All and repeatedly put it up for a vote and try to pass it, as they did for Obama-care and any other legislation they care about, we aren't going to have Medicare4All.

Do establishment Democrats expected Republicans, who are ideologically opposed to Medicare4All to put it up for a vote?
