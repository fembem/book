### Liberal Lies & gas-lighting

 An article of faith in the liberal bubble is that conservative
 and Republican leaders lie all the time, and do so purposefully, but that
 Democratic and liberal leaders don't often purposefully lie.

While Trump's incredible record of lies speaks for itself, that
doesn't change the fact that Democrats lie a lot too. Trump & Republicans
lying a lot more doesn't change that. Two things can both be big, while
one is a lot bigger than the other.

Here are several examples of repeated lies told by liberal leaders or by liberal media:
 - Trump supposedly saying Nazis are "fine people"
 - the 2021 Capitol rioters and intruders being armed
 - Brian Williams
 - Jayson Blair
 - Rachel Maddow
 - Hillary's lies about being under sniper fire
 - Hillary's lies about her past NAFTA support
 - Warren's lies about rejecting SuperPAC support
 - Biden's long history of lying

#### Trump saying Nazis were "fine people" at his August 15, 2017
  press conference at Trump Tower.

Despite almost universal unanimity among liberal media outlets and Democratic party
leaders, Trump did not say Nazis were "fine people".

It's clear from the context that he was referring to "fine people" being on both sides
of the Confederate statues debate on whether Confederate statues should be removed.

Here is the the [transcript](https://www.latimes.com/politics/la-na-pol-trump-charlottesville-transcript-20170815-story.html)
of that press conference where Trump supposedly said Nazis were "fine people", and here
is the [audio](https://www.youtube.com/watch?v=3nYLSAgj0Hk&t=165s).

Trump said:

> "Excuse me, excuse me. They didn't put themselves -- and you had some
> very bad people in that group, but you also had people that were very
> fine people, on both sides. You had people in that group. Excuse me,
> excuse me. I saw the same pictures as you did. You had people in that
> group that were there to protest the taking down of, to them, a very,
> very important statue and the renaming of a park from Robert E. Lee to
> another name."


One minute later, in response to the next media question, Trump said:

> "And you had people, and I'm not talking about the neo-Nazis and the white
> nationalists, because they should be condemned totally. But you had many
> people in that group other than neo-Nazis and white nationalists. OK?
> And the press has treated them absolutely unfairly."


Trump was clearly alleging that there were people who were only protesting
the taking down of a confederate statue and the renaming of a park. Whether
he was correct, mistaken, or lying, he didn't say Nazis were good people.

The fact that the first Charlottesville protests were organized by groups including
white supremacists and white nationalists, doesn't mean that all Charlottesville 
protests that arose thereafter were attended only by white nationalists and white
supremacists.

The context here is that protests and counter-protests in favor of and against
Confederate statues were occurring all over the country, and it's natural that
people might show up for a Charlottesville protest to support Confederate statues,
and not assume that all Charlottesville protests in favor of Confederate statues
would from then on be considered white nationalist protests, just because
the first ones had been organized by white nationalists.

Hypothetically, if Antifa organized the first protests against police
violence in a city, that wouldn't mean that all subsequent protests against
police violence in that city would be Antifa events, nor that anyone attending
such protests is a member or supporter of Antifa.

You can argue that Trump knew the original Charlottesville protests
were organized by white nationalists, and that you think his comments
therefore imply he thinks Nazis are "fine people", but that's not what
he said, it's at most what you think he implied.

There's a difference between what Trump said, and deducing something from what
Trump said. The deduction here is clearly debatable.

There's a difference between saying "Trump said" and "Trump implied" or
"Trump arguably implied".

The assertion that Trump said Nazis were fine people, assumes that:
 - Trump knew that all or almost all people at that rally were Nazis
 - all or almost all people who went to that rally were aware it was a Nazi rally
despite the lack of Nazi insignia

Those are both questionable assumptions.

For historical comparison, the "Million Man March" rally was co-organized
  by the Nation of Islam (NOI), regarding which the Southern Poverty Law Center
  [says](https://www.splcenter.org/fighting-hate/extremist-files/group/nation-islam)

> Its theology of innate black superiority over whites and the deeply
> racist, antisemitic and anti-LGBT rhetoric of its leaders have earned
> the NOI a prominent position in the ranks of organized hate.

And the Nation of Islam leader Louis Farrakhan gave the keynote speech of the rally.

Yet very few would argue that everyone or even the majority of people who attended
that event was necessarily anti-Semitic or anti-white.

#### Rachel Maddow

As relayed in this
[article](https://variety.com/2019/tv/news/rachel-maddow-russian-propaganda-claim-1203378854/)
in Variety magazine, Rachel Maddow said about OAN that

> "In this case, the most obsequiously
> pro-Trump right wing news outlet in America really literally is paid
> Russian propaganda."

Notice she used the words "literally" and "really".

Then she argued in court that her accusation was

> "...nothing more than a vivid, hyperbolic turn of phrase..."!
