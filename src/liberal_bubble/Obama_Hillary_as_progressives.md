 > "Obama is a black liberal, and Hillary is a female liberal, so they must be progressive"


This is illogical as it ignores an entire dimension of policy and progressivism, namely the economic policy dimension. Economic policy is a very important dimension as it encompasses things like wage justice, worker rights, financial rights, educational justice, healthcare justice, housing justice, etc, which we can collectively call "economic justice".

This is not to say social policy, which enompasses things like racial justice, gender justice, and sexual-orienation justice, is not important. Both economic justice and social justice are highly important.

Calling someone who is only socially progressive, a "progressive", is illogical, just like calling someone who is only economically progressive, a "progressive". Just like socially progressive people would not want someone who opposes all abortion rights to be considered a progressive, ecoonically progressive people likewise can make the same argument that someone who opposes universal healthcare is not a progressive.

Therefore, a liberal politician that is weak or has sold out on economic justice, is not an overall progressive, just socially progressive. If you are weak or sold out on an entire category of things that have major importance, then you are not a good progressive in general. Just like a high school student who gets A's in chemistry, biology, physics, and math, but gets C's in history, English, geography, civics, and social studies, can't claim to be a good student overall, the same is true for a politician that is weak or has sold out on economic policy.

Some politicians, including Hillary and Obama in the past, try to appear as being economically progressive by occasionally bringing up poverty, homelessness, pharmaceutical and healthcare costs, etc, but they almost always do so in the context of race or gender justice. In doing so, they are not supporting economic justice, just social justice.

When the only time a politician brings up poverty, homelessness, and drug and healthcare debt and bankruptcy, is to point out differences in outcomes between race and gender groups, what they are communicating is that they find those things acceptable, and just find the race and gender differences unacceptble.

While economic justice and social justice do clearly have overlap, that doesn't mean either is strictly part of the other. Just like it would be absurd to say that all or most social oppression is economic, it's also absurd to say that almost or all economic oppression is due to racism, sexism, homo-phobia, trans-phobia, fat-phobia, etc.

The reason for this is very simple: the rich and the super-rich have an incentive to keep as much money as they can, and to avoid paying higher taxes that could fund services to improive the lives of the poor and middle class, regardless of the race, gender, or other identity characteristic of those poor and middle-class people.

 - heroes of the "woke oligarchy" and "rainbow plutocracy"]