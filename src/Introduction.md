This book is about US politics and applying logic and consistency to political arguments.

This is not a book about psychology or how to convince people who are committed and invested to a narrative or ideology to disabuse themselves of their delusions, false arguments, and cherry-picked fact sets or anecdotes.

Nor is it a book about how to help you or those you debate with cope with the realization of having been wrong, how to be diplomatic when pointing out logical fallacies and incorrect assumptions, etc.

In any case, an interlocutor not being sufficiently diplomatic or sensitive, are often excuses and deflections raised by people who are coping with the dawning realization that they were wrong and have lost an argument.

Here are some of the other top excuses you will hear when you win arguments:
 - your are nitpicking
   - pointing out fallacious arguments and incorrect facts, isn't "nitpicking", it's debating
 - I don't care about this
 - nobody cares about this
 - it doesn't matter anyway
 - it's not precisely quantifiable, so it's pointless to talk or reason about
 - absolute truth is not knowable, so we can''t really know who's right or wrong

While that is a worthy topic, it is not the purpose of this book.

### Why logic matters

Without logic, our political discussions and debates would degenerate to:
 - seeing who can act more offended, hurt, or traumatized
 - seeing who's more personally successful or charitable
 - seeing who can collect or memorize more anecdotes that support their narrative
 - seeing whose argument has more reputable people on its side
 - seeing whose argument is more favorable to the most oppressed identity groups
 - etc

### Why consistency matters

Without consistency, debate and politics is just a never-ending game of making
the rules up as you go along. Even with logic, just as in math and science, we still need some basic assumptions or axioms.

In philosophy, sociology, and politics, we usually call axioms norms, conventions, truths, or laws.

Assumptions and axioms we need and that don't derive from logic are:

 - sentient life is valuable
 - animals have rights (i.e. not being tortured or abused)
 - the more sentient the animal, the more rights it has to not suffer
 - human life takes precedence over that of all other animals
 - killing humans for sport or enjoyment is wrong
 - not going out of one's way to avoid killing humans, is wrong
 - babies and the defenseless have a right to be protected
 - absent excessive chronic pain or a vegetative state, life is worth living
 - might does not make right
 - the law of the jungle doesn't apply to civilized society
 - one person's religious rights don't transcend the human, civil, or political rights of others

While these axioms can derived from game theory, economics, or the Golden Law (itself an axiom), we can also take them for granted because we all agree with them.

This book ir not an argument that logic is the only thing we need, but that where logic is being applied and where it is applicable, it should be done in a correct way, and not as a facade for dogma and orthodoxy.

When people want to make up the rules as they go along, if they can't get away with fooling you with logical fallacies, they may try to get away with getting rid of the axioms we all take for granted.

That's where consistency comes in. When someone tries to argue that an axiom doesn't apply in a situation, you can and should point out that they wouldn't want that axiom to not apply to other situations.

For example, if someone tries to argue that "might makes right" regarding the Israeli-Palestinian conflict, that person should be asked if that also applies to conflicts where the side they support is the weaker side.