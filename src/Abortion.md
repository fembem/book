Abortion is an issue where both sides of the political spectrum routinely present the issue as a binary choice between banning all abortions and allowing all abortions.

This false dichotomy reflects the false dichotomy between a fetus at any stage being a person with all the rights of personhood, and a woman having the right to abort a fetus at any stage and for whatever reason she deems appropriate.

The extreme elements on the right cosider a fertilized egg to have
the same rights as a newborn baby, while the extreme elements
on the left argue that a woman has a right to abort a fetus for whatever reason she chooses while that fetus is still inside the mother's body.

The US has some of the most permissive abortion laws in the world,
in fact so permissive that the laws of only 6 countries in the world are as permissive.

Politifact has rated "true" the statement that "The United States is one of only seven nations that allows elective abortions after 20 weeks post-fertilization."

Politifact's [fact check](https://www.politifact.com/factchecks/2015/may/26/family-foundation/family-foundation-says-us-among-seven-nations-allo/) says:

>The Family Foundation pointed us to a report on global abortion laws published in February 2014 by the Charlotte Lozier Institute, the research arm of the Susan B. Anthony List, an organization dedicated to ending abortion. It concluded that only seven of 198 nations and territories allow elective abortions after 20 weeks: Canada, China, Netherlands, North Korea, Singapore, United States and Vietnam.

>We came up with the same seven countries after sifting through 2014 data published by the Center for Reproductive Rights, a worldwide pro-abortion group.

So the fact that only Canada, China, Netherlands, North Korea, Singapore, and Vietnam have abortion laws as permissive as the United States, is acknowledged even by a worldwide pro-abortion group.

Given that 4 of those 6 countries are not democracies, the US doesn't seem to be in the best company on this issue, though one could say the same of Canada and Netherlands, which have widely respected human rights records.